<?php
if (!empty($_REQUEST)){
    if($_REQUEST["numero"]>0){
        $caso="bien";
    }
} else {
    $caso="mal"; // esta variable no será enviada al archivo ejercicio16.php
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            input[type="number"]{
                width: 300px;
            }
            .obligatorio::before{
                content: "Obligatorio";
                min-width: 150px;
                display: inline-block;
            }
            .noObligatoria:before{
                content: "opcional";
                min-width: 150px;
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <?php
        if($caso=="bien"){
            var_dump($_REQUEST);
        }else{
            ?>
            <form name="f"> <!-- se envia asi mismo -->
                <div class="obligatorio"><input required placeholder="Introduzca un número" step="1" min="1" max="100" type="number" name="numero" /></div>
                <div class="noObligatoria"><input placeholder="Introduzca otro número" step="1" min="1" max="100" type="number" name="numero1" /></div>
                <input type="submit" value="Enviar" name="boton" />
            </form>
        <?php
        }
        ?>
         
    </body>
</html>
