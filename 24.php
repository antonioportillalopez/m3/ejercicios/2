<!DOCTYPE html>
<!--
bucle switch
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $dia=date('l',time());
        switch ($dia){
            case "Monday":
                echo "Hoy es Lunes";
                break;
            case "Tuesday":
                echo "Hoy es Martes";
                break;
            case "Wednesday":
                echo "Hoy es Miércoles";
                break;
            case "Thursday":
                echo "Hoy es Jueves";
                break;
            case "Friday":
                echo "Hoy es Viernes";
                break;
            case "Saturday":
                echo "Hoy es Sábado";
                break;
            case "Sunday":
                echo "Hoy es Domingo";
                break;
            default:
                echo "No se";
        }
        ?>
    </body>
</html>
