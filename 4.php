<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $s;
        $j;
        $k;
        $i;
        $a=2.345;
        
        $s=0;
        for ($j=1;$j<=4;$j++)   {
            echo $j;
            if($j%2==0){ // $j%2 da el resto de la división entre dos es decir 
                $s+=$j;
            }
        }
        //$j queda con el valor de 5 al salir de bucle
        printf("<br>%d", $j); // printf(formato $d=decimal, %.1f float con un decimal,mas info de formatos en https://diego.com.es/formato-de-strings-en-php  )
        var_dump($s);
        $i=10;
        while ($i>0){
            $i=$i-1;
        }
        echo $i; // el contador cuando es -1 el bucle no lo ejecuta
        echo gettype($i).'<br>'; // dice de que tipo es la variable $i
        print_r($i); // muestra el valor de $i 
        var_dump($a); // muestra el valor de $a y dice de que tipo es la variable
        ?>
    </body>
</html>
